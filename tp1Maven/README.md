# TP MAVEN
                      
                      
## Description  


Ce tp est écrit en JAVA. Il a été une prise en main avec MAVEN et avait pour objectif primordial de comprendre le fonctionnement 
de ce dernier. Aussi son but était de savoir utiliser les artefacts, générer différents rapports maven , utiliser JEKINS pour 
l'intégration continu et aussi sauvegarder nos travaux pratiques (tp) sur le GIT. Il y'au eu aussi une introduction à SONAR et MOJO.



## Explications


Tout au debut de ce tp j'ai créé un projet basique maven que j'ai configuré par la suite afin que les plugins maven puissent 
bien marcher. Ensuite j'ai personnalisé le fichier pom.xml pour générer les rapports maven.
  
- JavaDocs Report : j'ai d'abord commenté mon code, puis ajouté le "maven-javadoc-plugin" au fichier pom.xml et après l'exécution,
   dans le site le rapport est directement généré avec tous les commentaires présents dans le code.
   
- CheckStyle REport: J'ai ajouté le plugin "checkstyle" qui génère le rapport concernant le style de code utilisé(indentation du 
   code par exemple). Il analyse le code et renvoit les violatons. Le plugin checkstyle est complémentaire avec le plugin JXR qui 
   permet de passer du rapport checkstyle au code source car pour chaque violations(erreurs) son numero de ligne dans le code lui ai
   associé. Il suffit donc juste de double cliquer sur le numero de ligne et on est directement renvoyé dans le code.

- Cobertura Test Coverage Report: ce rapport généré par le plugin "cobertura" permets de connaitre les parties de l'application
   qui n'ont pas été testées.
   
- PDM/CPD Report : Pour généré ces deux rapports j'ai ajouté le plugin PMD au fichier pom.xml. Le rapport PMD spécifie le code mort
   par exemple des variables déclarées mais non utilisées dans le code(la variable test dans mon application) et le rapport CPD identifie
   le code dupliqué(dans mon application il y'a pas de code dupliqué donc ce rapport ne renvoie aucune ligne).
   
- ChangeLog/File-activity/dev-activity Report : Ces trois rapports sont générés par le plugin "changelog" et "scm". Avec le scm on 
   passel'url d'une repository et après excétuion de l'application, le rapport ChangeLog liste tout ce qu'on fait sur le scm, le File-
   activité donne l'information sur le nombre de commits fait par chaque développeur, le nombre de fichiers modifiés et enfin, dev-
   activity le nombre de fichiers revisés.

Après avoir généré ces différents rapports, j'ai mis mon projet sur bitbucket, puis j'ai utilisé JENKINS pour compiler et éxécuter 
ce dernier. J'ai pas fais la partie SONAR et MOJO du tp.


   
   
   
