/**
 * 
 */
package fr.ifsic.tpj2ee;

/**
 * @author 13009797
 *
 */
public class PersonneBean {
	private java.lang.String name;
	private java.lang.String firstName;
	public PersonneBean() {
	}

	public java.lang.String getName() {
		return name;
	}

	public void setName(java.lang.String name) {
		this.name = name;
	}
	
	
	public java.lang.String getFirstName() {
		return firstName;
	}

	public void setFirstName(java.lang.String firstName) {
		this.firstName = firstName;
	}
}
