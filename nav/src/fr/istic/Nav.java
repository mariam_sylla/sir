package fr.istic;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Nav  extends HttpServlet {


	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		
		response.setContentType("text/html");
		PrintWriter out = null;;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			
			e.printStackTrace();
		}

		//chemin relatif
		RequestDispatcher requestBlue = request.getRequestDispatcher("blue.html");
		RequestDispatcher requestGreen = request.getRequestDispatcher("green.html") ;

		String st = request.getParameter("color");
		// si l'URlL selectionné est celle de la page bleu
		if(st.compareToIgnoreCase("blue") == 0) {
			requestBlue.forward(request, response);

		}
		// si l'URlL selectionné est celle de la page verte
		else 
		{
			requestGreen.forward(request, response);

		}
	}



}
