package fr.istic.tpjpa.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@DiscriminatorValue("E")
public class ElectronicDevice extends Peripherique {

	private String name;
	private Person person;

	public ElectronicDevice() {

	}

	public ElectronicDevice(String name, Person person) {

		this.name = name;
		this.person = person;
	}

	@ManyToOne
	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
