package fr.istic.tpjpa.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@DiscriminatorValue("H")
public class Heater extends Peripherique {

	private long puissance;
	private Home home;

	public Heater() {

	}

	public Heater(long puissance, Home home) {

		this.setPuissance(puissance);
		this.home = home;
	}

	@ManyToOne
	public Home getHome() {
		return home;
	}

	public void setHome(Home home) {
		this.home = home;
	}

	public long getPuissance() {
		return puissance;
	}

	public void setPuissance(long puissance) {
		this.puissance = puissance;
	}

}
