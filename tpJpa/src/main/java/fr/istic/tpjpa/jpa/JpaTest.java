package fr.istic.tpjpa.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import fr.istic.tpjpa.domain.ElectronicDevice;
import fr.istic.tpjpa.domain.Heater;
import fr.istic.tpjpa.domain.Home;
import fr.istic.tpjpa.domain.Person;

public class JpaTest {

	private EntityManager manager;

	public JpaTest(EntityManager manager) {
		this.manager = manager;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("example");
		EntityManager manager = factory.createEntityManager();
		JpaTest test = new JpaTest(manager);

		EntityTransaction tx = manager.getTransaction();
		tx.begin();

		// create entity

		// Entity Person

		Person person1 = new Person();
		person1.setNom("Sylla");
		person1.setPrenom("Mariam");
		person1.setGenre("Feminin");
		person1.setMail("mariamsylla@yahoo.fr");
		person1.setProfilFacebook("Mariam");

		Person person2 = new Person();
		person2.setNom("Dupont");
		person2.setPrenom("Jean");
		person2.setGenre("Masculin");
		person2.setMail("jeandupont@yahoo.fr");
		person2.setProfilFacebook("Jean");

		Person person3 = new Person();
		person3.setNom("Cloarec");
		person3.setPrenom("Nolann");
		person3.setGenre("Masculin");
		person3.setMail("nolanncloarec@yahoo.fr");
		person3.setProfilFacebook("nolann");

		// Entity Home
		Home home1 = new Home();
		home1.setAdresse("32, rue mirabeau, 35700 Rennes");
		home1.setAdresseIP("00.00.00");
		home1.setSuperficie(20);

		Home home2 = new Home("29, Avenue Prof Charles Foulon, 35000 Rennes",
				40, "01.01.01");
		Home home3 = new Home("44, Rue de Nantes, 35000 Rennes", 30, "02.02.02");

		// Entity Heater
		Heater heater1 = new Heater();
		heater1.setPuissance(1000);
		heater1.setCaracteristique("chauffage1");

		Heater heater2 = new Heater();
		heater2.setPuissance(1500);
		heater2.setCaracteristique("chauffage2");

		Heater heater3 = new Heater();
		heater3.setPuissance(1500);
		heater3.setCaracteristique("chauffage3");

		// Entity Electronic Device
		ElectronicDevice device1 = new ElectronicDevice();
		device1.setName("four");
		device1.setCaracteristique("equipement1");

		ElectronicDevice device2 = new ElectronicDevice();
		device2.setName("microndes");
		device2.setCaracteristique("equipement2");

		person1.getFriends().add(person2);
		person1.getFriends().add(person3);
		home1.getHeaters().add(heater1);
		home1.getHeaters().add(heater2);
		home2.getHeaters().add(heater3);
		person1.getHomes().add(home1);
		person1.getHomes().add(home2);
		person1.getDevices().add(device2);
		person2.getDevices().add(device1);

		heater1.setHome(home1);
		heater2.setHome(home1);
		heater3.setHome(home2);
		home1.setPerson(person1);
		home2.setPerson(person1);
		device2.setPerson(person1);
		device1.setPerson(person2);

		// persist entity

		manager.persist(person1);
		manager.persist(person2);
		manager.persist(person3);
		manager.persist(device1);
		manager.persist(device2);
		manager.persist(home1);
		manager.persist(home2);
		manager.persist(home3);
		manager.persist(heater1);
		manager.persist(heater2);
		manager.persist(heater3);

		tx.commit();

		// run request
		System.out.println("LISTE DES PERSONNES PRESENTES DANS LA BASE");
		test.getPersons();
		System.out.println("LISTE DES MAISONS PRESENTES DANS LA BASE");
		test.getHomes();
		System.out
				.println("LISTE DES MAISONS QUI ONT UN CHAUFFAGE DE CAPACITE 1000");
		test.getPuissanceHome();
		System.out.println("LISTE DES MAISONS SANS HABITANTS");
		test.getHomeEmpty();
		System.out.println("LISTE DES PERSONNES QUI N'ONT PAS DE MAISON");
		test.getPersonNoHome();
		System.out
				.println("LISTE DES EQUIPEMENTS ELECTRONIQUES DONT LE NOM COMMENCE PAR F");
		test.getElectronicDevice();
		System.out.println("LA PERSONNE DONT LE NOM EST SYLLA");
		test.getPersonNameParam("Sylla");
		System.out
				.println("REQUETE NOMMEE: LISTE DES PERSONNE ORDONNEE PAR NOM");
		test.getNamedQueries();

		manager.close();
		factory.close();
		System.out.println(".. done");

	}

	public void getPersons() {

		Query query = manager.createQuery("select p from Person p",
				Person.class);
		List<Person> persons = query.getResultList();
		System.out.println("nombre  de personne:" + persons.size());
		for (Person next : persons) {
			System.out.println("next personne: " + next.toString());
		}
	}

	public void getHomes() {

		Query query = manager.createQuery("select h from Home h", Home.class);
		List<Home> homes = query.getResultList();
		System.out.println("nombre  de maison:" + homes.size());
		for (Home next : homes) {
			System.out.println("next maison: " + next.toString());
		}
	}

	public void getPuissanceHome() {
		Query query = manager
				.createQuery(
						"select h from Home h join  h.heaters as heat where heat.puissance = 1000",
						Home.class);
		List<Home> homes = query.getResultList();
		for (Home next : homes) {
			System.out.println("la maison est : " + next.toString());
		}

	}

	public void getHomeEmpty() {
		Query query = manager.createQuery(
				"select h from Home h  where h.person is null", Home.class);
		List<Home> homes = query.getResultList();
		for (Home next : homes) {
			System.out.println("la maison est : " + next.toString());
		}

	}

	public void getPersonNoHome() {
		Query query = manager
				.createQuery("select p from Person p  where size(p.homes) = 0",
						Person.class);
		List<Person> persons = query.getResultList();
		for (Person next : persons) {
			System.out.println("la personne est : " + next.toString());
		}

	}

	public void getElectronicDevice() {
		Query query = manager.createQuery(
				"select d from ElectronicDevice d  where d.name like 'f%'",
				ElectronicDevice.class);
		List<ElectronicDevice> devices = query.getResultList();
		for (ElectronicDevice next : devices) {
			System.out.println("l'id de l'appareil est " + next.getId()
					+ " et le nom est " + next.getName());
		}

	}

	public void getPersonNameParam(String name) {
		Query query = manager.createQuery(
				"select p from Person p  where p.nom = :name", Person.class);
		query.setParameter("name", name);
		List<Person> persons = query.getResultList();
		for (Person next : persons) {
			System.out.println("la personne est : " + next.toString());
		}

	}

	public void getNamedQueries() {
		Query query = manager.createNamedQuery("liste des personnes par nom ");

		List<Person> persons = query.getResultList();
		for (Person next : persons) {
			System.out.println("la personne est : " + next.toString());
		}

	}

}
