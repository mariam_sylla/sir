# TP JPA


## Description

Ce tp est écrit en JAVA.Il a pour but de mieux comprendre la persistance des données, l'accès à une base de données
relationnelle et aussi la gestion de la base de données.


## Explications

Dans un premier temps j'ai créé deux entités(objets métiers) qui sont les classes <<Person et Home>>. Toutes ces deux classes 
possédent un attribut <<id>> qui servira de clé primaire dans la base de données et d'autres champs qui sont associés à des 
colonnes en base. Aussi toutes ces deux classes ont un constructeur vide par défaut.Elles sont nommées <<ENTITY BEAN>> en JAVA, 
cela veut dire que les instances de ces classes peuvent être persistantes. C'est l'annotation @Entity qui permet d'indiquer cela.
  
          @Entity              
          public class Person {
	  
Sur le getteur de l'attribut id de ces classes, j' ai ajouté une annotation ce qui permet pas un accès direct aux champs en cas de 
lecture ou d'ecriture, il faut donc passer par les getteurs et les setteurs.

- Exemple:

          @Id                  
          @GeneratedValue      
	      public Long getId() {
          return id; 
          }         

Plus loin, j'ai créé deux nouvelles entités <<Heater et ElectronicDevice>> afin de rendre mon modèle plus complexe.Dans ce modèle:

 - Une personne peut avoir plusieurs maisons, plusieurs appareils electronics et aussi des amis
 - Une maison quand à elle  a aussi une liste de chauffages
 - Un chauffage ne peut être dans deux maisons à la fois idem pous les appreils electronics et personne

Pour réaliser ces différents liens, je me suis servie des annotations permettant de faire le mapping. Par exemple entre les classes
Person et Home vous pouvez coir ceci:

- Dans Person 

             @OneToMany(mappedBy = "person", cascade = CascadeType.PERSIST)
	         public List<Home> getHomes() {                                
             return homes;                                                 
	         }                                                             
          
- Dans Home 

             @ManyToOne                 
	         public Person getPerson() {
             return person;             
             }                        
          
J'ai ensuite créer des instances de toutes ces classes dans une autre classe service JpaTest permettant de tester mon application.
A l'aide des setteurs,j'ai rempli les champs de ces dernières et je les ai rendues persistantes avec l'objet Entitymanager. 
Cet objet va permettre de les gérer (ajout, modification,interrogation...).

Pour la connection à la base de données j'ai utilisé le fichier de configuration  persistance.xml, ce fichier definit <<Hibernate>>
comme fournisseur pour la persistance et qui configure l'accès à la base de données via le driver, l'url, le nom de la base, le nom 
de l'utilisateur et le mot de passe.Dans la dernière version de persistance.xml vous trouverez la configuration de la connexion à
une base de données MYSQL que j'ai créée sur anteros.

		     <property name="hibernate.connection.driver_class" value="com.mysql.jdbc.Driver" />
             <property name="hibernate.connection.password" value="********" />                 
		     <property name="hibernate.connection.url"                                          
		      value="jdbc:mysql://mysql.istic.univ-rennes1.fr/base_13009797" />                  
             <property name="hibernate.connection.username" value="user_13009797" />            



En ce qui concerne l'heritage, j'ai choisi la table de jointure. J'ai créé une nouvelle classe <<Peripherique>> et les classes Heater
et electronicDevice vont heriter ce dernier. Avec ce type d'heritage les deux classes n'auront plus d'attributs id , elles auront en
en commun le champs id de la table mère (Périphérique)qui s'occupera de faire la jointure. Les champs de chacune des classes sont 
sauvegardés dans les classes elles mêmes.Pour les rêquetes j'ai utilisé le JPQL comme vu en TD (voir la classe JpaTest).