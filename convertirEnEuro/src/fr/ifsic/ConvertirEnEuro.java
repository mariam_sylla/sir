package fr.ifsic;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ConvertirEnEuro extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest req, HttpServletResponse response)
			throws IOException, ServletException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<html>");
		out.println("<head><title>Convertir en Euro</title></head>");
		out.println("<body>");
		out.println("<h1>R�sultat</h1>");
		int somme = Integer.parseInt(req.getParameter("somme"));
			out.println(somme + " Frs valent ");
			out.println(somme / 6.55957 + " Euro");			
		out.println("</body></html>");
		

	}
} // fin class ConvertirEnEuro
