# TP NOSQL REDIS

Redis est un type de base de données NoSQL qui permet de manipuler de simples données et des ensembles ordonnées et stocke ces
données sous forme de clé/valeur.

## Exemple 1

              public class App
              {
              public static void main( String[] args )
              {
	              Jedis jedis = new Jedis("localhost");
	              jedis.set("foo", "bar");
	              String value = jedis.get("foo");
	
	              System.err.println(value);	
              }
              }
			  
			  
Dans ce exemple, on stocke une chaine de caractère <<bar>> avec pour clé <<foo>>. Et en faisant un get sur la clé, la valeur qui 
lui ai associée c'est-à-dire bar est retournée.Ca se comporte comme un hashmap.


## Exemple 2


             public static void main(String[] args) 
			 {
		         Jedis jedis = new Jedis("localhost");
                 System.out.println(jedis.get("counter"));
		         jedis.incr("counter");
		         System.out.println(jedis.get("counter"));
	         }

Dans cet autre exemple, le premier <<System.out.println(jedis.get("counter"))>> retourne null vue qu'il y'a aucune valeur qui lui ai
associée. le deuxième va retourner 1 car l'instruction <<jedis.incr("counter")>> definit directement le type (int ou long) du champ 
devant être associé à la clé <<counter>>, l'initialise à 0 puis l'incrémente ce qui donne la valeur 1.


## Exemple 3

             public static void main(String[] args) throws InterruptedException 
             {
		         String cacheKey = "cachekey";
		         Jedis jedis = new Jedis("localhost");
		         // adding a new key
		         jedis.set(cacheKey, "cached value");
		         // setting the TTL in seconds
		         jedis.expire(cacheKey, 15);
		         // Getting the remaining ttl
		         System.out.println("TTL:" + jedis.ttl(cacheKey));
		         Thread.sleep(3000);
		         System.out.println("TTL:" + jedis.ttl(cacheKey));
		         // Getting the cache value
		         System.out.println("Cached Value:" + jedis.get(cacheKey));

		         // Wait for the TTL finishs
		         Thread.sleep(15000);

		         // trying to get the expired key
		         System.out.println("Expired Key:" + jedis.get(cacheKey));
	          }
              
Ici, l'instruction <<jedis.expire(cacheKey, 15)>> fixe la durée de vie de la donnée à 15s et après chaque 3000ms(3s), on affiche
la valeur associé à clé <<Thread.sleep(3000)>> ce qui donne 12 (15 - 3). Et après les 15000ms = 15s la donnée est détruite vu que 
sa durée de vie était fixée à 15 ms ce qui renvoie donc null.


## Exemple 4

 
            public static void main(String[] args) 
            {
               String cacheKey = "languages";
              Jedis jedis = new Jedis("localhost");
              // Adding a set as value

		      jedis.sadd(cacheKey, "Java");
		      jedis.sadd(cacheKey, "C#");
		      jedis.sadd(cacheKey, "Python");// SADD

		      // Getting all values in the set: SMEMBERS
		      System.out.println("Languages: " + jedis.smembers(cacheKey));
		      // Adding new values
		      jedis.sadd(cacheKey, "Java");
		      jedis.sadd(cacheKey, "Ruby");
		      // Getting the values... it doesn't allow duplicates
		      System.out.println("Languages: " + jedis.smembers(cacheKey));

           }
           
           
           
Dans ce dernier exemple, une liste de chaine de caractère est associée à la clé <<cacheKey>> et le syso affiche un ensemble (Set) dont
les éléments sont de types chaines de caractère: C#,Java,Python,Ruby


## Types de données stockés dans Redis 

Redis est une grosse HashMap qui permet le stockage des données structurées telles que :

- Les chaines de caractère
- Les Listes
- Les Hash 
- Les sets (ensembles)
- Les sets trié (ensembles ordonnés)

L’utilisation de redis est très simple et la vitesse de lecture et d’écriture est vertigineuse (très rapide). Il n'ya pas un problème
de concurrence d'accès aux données car il y'a atomicité des opérations.

Avec Redis on peut pas faire de requête sur les valeus des champs comme on le fait avec la clause where en MySQL.Pour avoir la valeur 
d'un champ il faut passer par sa clé.












