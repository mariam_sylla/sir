package fr.istic.univ_rennes1.miage.tpNoSQL;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bson.types.ObjectId;

import com.google.code.morphia.annotations.Entity;
import com.google.code.morphia.annotations.Id;
import com.google.code.morphia.annotations.Reference;

@Entity
public class Article {
	
@Id ObjectId id;
 String name;
 int stars;
 @Reference List<Person> buyers = new ArrayList<Person>();
 
 public Article(String name, int stars, List<Person> buyers) {
		this.name = name;
		this.stars = stars;
		this.buyers = buyers;
	}
 
 public Article() {
	 
 }

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public int getStars() {
	return stars;
}

public void setStars(int stars) {
	this.stars = stars;
}

public List<Person> getBuyers() {
	return buyers;
}

public void setBuyers(List<Person> buyers) {
	this.buyers = buyers;
}
 
 

}
