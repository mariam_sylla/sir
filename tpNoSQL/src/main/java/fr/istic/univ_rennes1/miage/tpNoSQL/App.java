package fr.istic.univ_rennes1.miage.tpNoSQL;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.code.morphia.Datastore;
import com.google.code.morphia.Morphia;
import com.mongodb.Mongo;

/**
 * main
 * 
 */
public class App {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) throws UnknownHostException {

		Morphia morphia = new Morphia();

		Mongo mongo = new Mongo();

		morphia.map(Person.class).map(Address.class);

		Datastore ds = morphia.createDatastore(mongo, "my_database");

		// set address
		Address address1 = new Address();
		address1.setStreet("29 av General leclerc");
		address1.setCity("Rouen");
		address1.setPostCode("76000");
		address1.setCountry("France");

		Address address2 = new Address();
		address2.setStreet("32 Rue Mirabeau");
		address2.setCity("Rennes");
		address2.setPostCode("35700");
		address2.setCountry("France");

		Address address3 = new Address();
		address3.setStreet("10 rue Georges Lebigot");
		address3.setCity("paris");
		address3.setPostCode("75001");
		address3.setCountry("France");

		Address address4 = new Address();
		address4.setStreet("8 av Charles De Gaule");
		address4.setCity("Villejuif");
		address4.setPostCode("94800");
		address4.setCountry("France");

		List<Address> adresses1 = new ArrayList<Address>();
		adresses1.add(address1);
		adresses1.add(address2);

		List<Address> adresses2 = new ArrayList<Address>();
		adresses2.add(address3);
		adresses2.add(address4);

		List<Address> adresses3 = new ArrayList<Address>();
		adresses3.add(address1);

		// set person
		Person p1 = new Person();
		p1.setName("Jean");

		Person p2 = new Person();
		p2.setName("Mariam");

		Person p3 = new Person();
		p3.setName("Dupond");

		Person p4 = new Person();
		p4.setName("Zineb");

		p1.setAdresses(adresses1);
		p2.setAdresses(adresses2);
		p3.setAdresses(adresses3);
		p4.setAdresses(adresses1);

		List<Person> persons1 = new ArrayList<Person>();
		persons1.add(p1);
		persons1.add(p2);

		List<Person> persons2 = new ArrayList<Person>();
		persons2.add(p3);

		List<Person> persons3 = new ArrayList<Person>();
		persons3.add(p2);
		persons3.add(p4);

		// set Article
		Article art1 = new Article();
		art1.setName("ordinateur");
		art1.setStars(250);

		Article art2 = new Article();
		art2.setName("Telephone");
		art2.setStars(800);

		Article art3 = new Article();
		art3.setName("accesoire pc");
		art3.setStars(1000);

		art1.setBuyers(persons1);
		art2.setBuyers(persons2);
		art3.setBuyers(persons3);

		// Save the POJO
		ds.save(address1);
		ds.save(address2);
		ds.save(address3);
		ds.save(address4);
		ds.save(p1);
		ds.save(p2);
		ds.save(p3);
		ds.save(p4);
		ds.save(art1);
		ds.save(art2);
		ds.save(art3);

		System.out.println("====PERSONNES ET LEURS ADRESSES====");
		for (Person e : ds.find(Person.class)) {
			System.err.println();
			for (int i = 0; i < e.getAdresses().size(); i++) {

				System.out.println("La personne " + e.getName()
						+ " habite l'adresse : "
						+ e.getAdresses().get(i).getStreet() + " "
						+ e.getAdresses().get(i).getCity() + " "
						+ e.getAdresses().get(i).getPostCode() + " "
						+ e.getAdresses().get(i).getCountry());
			}
		}

		System.out.println("");

		System.out.println("====ARTICLES ET LEURS ACHETEURS====");
		for (Article art : ds.find(Article.class)) {
			for (int i = 0; i < art.getBuyers().size(); i++) {
				System.out.println("l'article " + art.getName()
						+ " avec une quantité " + art.getStars()
						+ " est acheté par Personne: "
						+ art.getBuyers().get(i).getName());

			}
		}

	}
}
