package fr.istic.univ_rennes1.miage.tpNoSQL;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;

import com.google.code.morphia.annotations.Entity;
import com.google.code.morphia.annotations.Id;
import com.google.code.morphia.annotations.Reference;

@Entity
public class Person {

	@Id
	ObjectId id;
	String name;
	@Reference
	List<Address> address = new ArrayList<Address>();

	public Person(String name, List<Address> address) {
		this.name = name;
		this.address = address;
	}

	public Person() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Address> getAdresses() {
		return address;
	}

	public void setAdresses(List<Address> address) {
		this.address = address;
	}

}
