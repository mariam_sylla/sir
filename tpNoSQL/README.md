# TP NOSQL MONGODB


## Description

Ce tp a consisté à la crétion d'une application simple utilisant une base de données MongoDB. Il a permis de comprendre le 
fonctionnement des bases NoSQL, mais aussi les avantages et les limtes de ces types de base de données.


## Explications

En premier lieu j'ai d'abord installé la base de données MongoDB en suivant les différentes instructions données sur l'énoncé du tp,
afin de mettre en place l'environnement. MongoDB est une base de données orientée document qui permet de stocker des données ayant 
une structure variable. Les données sont sous formes de documents qui sont enregistrés  dans des collections(table en BD relationnelle).
Lorsqu'on veut ajouter par exemple une colonne à un document on a pas besoin d'une mise à jour du schema car avec ce type BD on peut avoir 
des champs différents au sein d'une collection.Ensuite j'ai crée une application (projet Maven) utilisant MORPHIA, une API proche du JPA qui 
va permettre la persistance et la gestion des entités créées.
Dans cette application, j'ai créé trois classes <<Article,Person et adress>> qui respectent le modèle suivant:

- Un article a plusieurs acheteurs
- Un acheteur(personne) a à son tour plusieurs adresses

J'ai indiqué ces différents lien avec les annotations Morphia qui sont un peu différentes de celles, de JPA car avec Morphia on ne 
peut pas mettre d'annotion sur les getteurs et setteurs. Par exemple pour le champs de la clé et les mapping au niveau des listes 
on utilise :
     
	     @Id
	     ObjectId id;
	     @Reference
	     List<Address> address = new ArrayList<Address>();
	  
Et pour finir j'ai créé des instances de ces classes pour alimenter la base de données et tester mes requêtes.

- REMARQUE : Avec Morphia il faut persister(save) toutes les instances contrairement à JPA lorsque qu'on avait par exemple 
     person.add(home) en rendant persistante l'instance de Person, celle de Home l'est directement aussi.
	

## Avantages et limites

### Avantages

- Performance dans le temps de restitution
- Manipulation rapide des données malgré la charge et le volume important de ces derniers
- Insertion des structures de données différentes dans les collections ou Modification des structures existantes de manière 
      transparentes sans impact sur les autres entrées(plus de alter table)

### Limites

- Manque d'intégrité transactionnelle importante pour certaines fonctionnalités


