package fr.istic;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class TestContextes extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
	{
		response.setContentType("text/html");
		PrintWriter out = null;;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//obtention contexte de l'application
		ServletConfig config = this.getServletConfig();
		ServletContext application = config.getServletContext();
		//extraire le nb de session
		Integer nbSession = (Integer) application.getAttribute("nbSession");

		//obtenir la session
		HttpSession session = request.getSession();
		Integer nbResquest = (Integer) session.getAttribute("nbResquest");

		if (nbResquest == null) {
			if(nbSession == null) {
				synchronized (this) {
					application.setAttribute("nbSession",1);
				}
			}
			else
			{
				synchronized (this) {
					application.setAttribute("nbSession",nbSession+1);
				}
			}
			session.setAttribute("nbResquest",1);
			session.setAttribute("numSession",application.getAttribute("nbSession"));
		}
		else 
		{
			session.setAttribute("nbResquest",nbResquest+1); 
		}
		
       //Teste si le contexte de requ�te est toujours vierge
		if (request.getAttribute("count") == null){
			request.setAttribute("count", 1);
		}
		else {

			request.setAttribute("count", (Integer)request.getAttribute("count")+1);
		}
		
		//Dur�e de la session
		session.setMaxInactiveInterval(300);
		
		out.println("<html>");
		out.println("<body>");
		out.println("<head>");
		out.println("<title>Contextes</title>");
		out.println("</head>");
		out.println("<body>");
		out.println("<h1>Utilisations des contextes</h1>");
		out.println("le nombre de sessions depuis le debut = "+application.getAttribute("nbSession"));
		out.println("<br>");
		out.println("Votre numero de session est : "+session.getAttribute("numSession"));
		out.println("<br>");
		out.println("le nombre de requetes dans la session = "+session.getAttribute("nbResquest"));
		out.println("<br>");
		out.println("(Request Context) count = "+request.getAttribute("count"));
		out.println("</body>"); 
		out.println("</html>");
	}

}
