package fr.istic.univ_rennes1.miage.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jdo.annotations.Extension;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

@Entity
public class Person implements Serializable{

	private static final long serialVersionUID = -8853790854567956705L;
	
	private Long id;
	private String nom;
	private String prenom;
	private String genre;
	
	private String mail;
	private String profilFacebook;

	private List<Home> homes = new ArrayList<Home>();
	//private List<Person> friends = new ArrayList<Person>();
//	private List<ElectronicDevice> devices = new ArrayList<ElectronicDevice>();

	public Person() {

	}

	public Person(String nom, String prenom, String genre, String mail,
			String profilFacebook) {

		this.nom = nom;
		this.prenom = prenom;
		this.genre = genre;
		this.mail = mail;
		this.profilFacebook = profilFacebook;
		
	}

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Extension(vendorName = "datanucleus", key = "gae.encoded-pk", value = "true")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getProfilFacebook() {
		return profilFacebook;
	}

	public void setProfilFacebook(String profilFacebook) {
		this.profilFacebook = profilFacebook;
	}

	

	@Override
	public String toString() {
		return "Personne [id=" + id + ", Nom=" + nom + ", prenom=" + prenom
				+ ", genre=" + genre
				+ ",mail=" + mail + ",profilFacebook=" + profilFacebook + " ]";

	}

	@OneToMany(mappedBy = "person", cascade = CascadeType.PERSIST)
	public List<Home> getHomes() {
		return homes;
	}

	public void setHomes(List<Home> homes) {
		this.homes = homes;
	}

//	@ManyToMany
//	public List<Person> getFriends() {
//		return friends;
//	}
//
//	public void setFriends(List<Person> friends) {
//		this.friends = friends;
//	}
//
//	@OneToMany(mappedBy = "person", cascade = CascadeType.PERSIST)
//	public List<ElectronicDevice> getDevices() {
//		return devices;
//	}
//
//	public void setDevices(List<ElectronicDevice> devices) {
//		this.devices = devices;
//	}

}
