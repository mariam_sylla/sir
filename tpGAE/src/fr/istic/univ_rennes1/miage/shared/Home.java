package fr.istic.univ_rennes1.miage.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Home  implements Serializable{
	private static final long serialVersionUID = -8853790854567956705L;

	private Long id;
	private String adresse;
	private long superficie;
	private String adresseIP;
	private Person person;
	//private List<Heater> heaters = new ArrayList<Heater>();

	public Home() {

	}

	public Home(String adresse, long superficie, String adresseIP) {
		this.adresse = adresse;
		this.superficie = superficie;
		this.adresseIP = adresseIP;
	}

	@Id
	@GeneratedValue
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public long getSuperficie() {
		return superficie;
	}

	public void setSuperficie(long superficie) {
		this.superficie = superficie;
	}

	public String getAdresseIP() {
		return adresseIP;
	}

	public void setAdresseIP(String adresseIP) {
		this.adresseIP = adresseIP;
	}

	@Override
	public String toString() {
		return "Maison [id=" + id + ", Adresse=" + adresse + ", Superficie="
				+ superficie + ",Adresse IP=" + adresseIP + " ]";
	}

//	@OneToMany(mappedBy = "home", cascade = CascadeType.PERSIST)
//	public List<Heater> getHeaters() {
//		return heaters;
//	}
//
//	public void setHeaters(List<Heater> heaters) {
//		this.heaters = heaters;
//	}

	@ManyToOne
	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

}
