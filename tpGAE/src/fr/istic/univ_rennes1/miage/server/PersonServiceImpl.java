package fr.istic.univ_rennes1.miage.server;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.servlet.ServletException;

import com.google.apphosting.api.DatastorePb.CompiledQuery.EntityFilter;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.istic.univ_rennes1.miage.client.PersonService;
import fr.istic.univ_rennes1.miage.shared.Home;
import fr.istic.univ_rennes1.miage.shared.Person;

@SuppressWarnings("serial")
public class PersonServiceImpl extends RemoteServiceServlet implements PersonService {

	EntityManager manager;
	
	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
		manager = EMF.get().createEntityManager();
		
	}
	
	@Override
	public void deletePerson(Person p) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public Person addPerson(String nom, String prenom, String genre,
			String mail, String profilFacebook) {
		Person p = new Person(nom, prenom, genre, mail, profilFacebook);
		EntityTransaction t =  manager.getTransaction();
		if (!t.isActive())
			t.begin();
		manager.persist(p);
		t.commit();
		
		return p;
	}

	@Override
	public Home addHome(String adresse, long superficie, String adresseIP) {
     Home h = new Home(adresse, superficie, adresseIP);
		
		EntityTransaction t =  manager.getTransaction();
		if (!t.isActive())
			t.begin();
		manager.persist(h);
		t.commit();
		
		return h;
	}

	

}
