package fr.istic.univ_rennes1.miage.client;


import fr.istic.univ_rennes1.miage.shared.Home;
import fr.istic.univ_rennes1.miage.shared.Person;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class TpGAE implements EntryPoint {
	/**
	 * The message displayed to the user when the server cannot be reached or
	 * returns an error.
	 */
	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network "
			+ "connection and try again.";

	/**
	 * Create a remote service proxy to talk to the server-side Greeting service.
	 */
	private final PersonServiceAsync personService = GWT.create(PersonService.class);

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		
		
		HorizontalPanel p1 = new HorizontalPanel();
		HorizontalPanel p2 = new HorizontalPanel();
		HorizontalPanel p3 = new HorizontalPanel();
		HorizontalPanel p4 = new HorizontalPanel();
		HorizontalPanel p5 = new HorizontalPanel();
		HorizontalPanel p6 = new HorizontalPanel();
		
		VerticalPanel v1 = new VerticalPanel();
		VerticalPanel v2 = new VerticalPanel();
		VerticalPanel v3 = new VerticalPanel();
		VerticalPanel v4 = new VerticalPanel();
		VerticalPanel v5 = new VerticalPanel();
		VerticalPanel v6 = new VerticalPanel();
		
		
		Button b = new Button("create");
		
		Label l1 = new Label("Nom");
		final TextBox t1 = new TextBox();
		p1.setSpacing(10);
		p1.add(l1);
		p1.add(t1);
		v1.add(p1);
		
		Label l2 = new Label("Prenom");
		final TextBox t2 = new TextBox();
		p2.setSpacing(10);
		p2.add(l2);
		p2.add(t2);
		v2.add(p2);
		
		Label l3 = new Label("Genre");
		final TextBox t3 = new TextBox();
		p3.setSpacing(10);
		p3.add(l3);
		p3.add(t3);
		v3.add(p3);
		
		Label l4 = new Label("Mail");
		final TextBox t4 = new TextBox();
		p4.setSpacing(10);
		p4.add(l4);
		p4.add(t4);
		v4.add(p4);
		
		Label l5 = new Label("Profil facebook");
		final TextBox t5 = new TextBox();
		p5.setSpacing(10);
		p5.add(l5);
		p5.add(t5);
		v5.add(p5);
		
		p6.setSpacing(10);
	
		p6.add(b);
		v6.add(b);
		
		RootPanel.get().add(v1);
		RootPanel.get().add(v2);
		RootPanel.get().add(v3);
		RootPanel.get().add(v4);
		RootPanel.get().add(v5);
		RootPanel.get().add(v6);
		
      b.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				
			//personService.addPerson(t1.getText(), t2.getText(),t3.getText(), t4.getText(), t5.getText(), new AsyncCallback<Person>() {
			
			personService.addHome("32 rue mirabeau",50,"01.01.01", new AsyncCallback<Home>() {
					@Override
					public void onSuccess(Home result) {
						//Window.alert("La person" + result.getNom() + " a �t� cr�� avec succ�s");
						Window.alert("La maison a �t� cr�� avec succ�s");
					}
					
					@Override
					public void onFailure(Throwable caught) {
						
					}
				});
			}
			
		});
      
      
   		
	}
}
