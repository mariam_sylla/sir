package fr.istic.univ_rennes1.miage.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.istic.univ_rennes1.miage.shared.Home;
import fr.istic.univ_rennes1.miage.shared.Person;

public interface PersonServiceAsync {

	void addPerson(String nom, String prenom,String genre,String mail,String profilFacebook, AsyncCallback<Person> callback);

	void deletePerson(Person p, AsyncCallback<Void> callback);

	void addHome(String adresse, long superficie, String adresseIP,
			AsyncCallback<Home> callback);

}
