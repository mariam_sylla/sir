package fr.istic.univ_rennes1.miage.client;

import java.util.Iterator;

import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

public class EntryPointHome extends Panel {

	HorizontalPanel p1 = new HorizontalPanel();
	HorizontalPanel p2 = new HorizontalPanel();
	HorizontalPanel p3 = new HorizontalPanel();
	HorizontalPanel p4 = new HorizontalPanel();

	VerticalPanel v1 = new VerticalPanel();
	VerticalPanel v2 = new VerticalPanel();
	VerticalPanel v3 = new VerticalPanel();
	VerticalPanel v4 = new VerticalPanel();
	
   public EntryPointHome() {
	Button b = new Button("create");
	Label l1 = new Label("Adresse");
	final TextBox t1 = new TextBox();
	p1.setSpacing(10);
	p1.add(l1);
	p1.add(t1);
	v1.add(p1);

	Label l2 = new Label("Superficie");
	final TextBox t2 = new TextBox();
	p2.setSpacing(10);
	p2.add(l2);
	p2.add(t2);
	v2.add(p2);

	Label l3 = new Label("AdresseIP");
	final TextBox t3 = new TextBox();
	p3.setSpacing(10);
	p3.add(l3);
	p3.add(t3);
	v3.add(p3);

	p4.add(b);
	v4.add(b);

	RootPanel.get().add(v1);
	RootPanel.get().add(v2);
	RootPanel.get().add(v3);
	RootPanel.get().add(v4);

   }
	@Override
	public Iterator<Widget> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean remove(Widget child) {
		// TODO Auto-generated method stub
		return false;
	}


}
