package fr.istic.univ_rennes1.miage.client;



import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.istic.univ_rennes1.miage.shared.Home;
import fr.istic.univ_rennes1.miage.shared.Person;

@RemoteServiceRelativePath("person")
public interface PersonService extends RemoteService{
	
	Person addPerson(String nom, String prenom,String genre,String mail,String profilFacebook);
	Home addHome(String adresse,long superficie,String adresseIP);
	void deletePerson(Person p);
	

}
